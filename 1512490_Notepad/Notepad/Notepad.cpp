﻿// Notepad.cpp : Defines the entry point for the application.
//
#include "stdafx.h"
#include "Notepad.h"
#include <Windows.h>
#include <commctrl.h>
#include <commdlg.h>
#include "Resource.h"
#pragma comment(linker,"\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#pragma comment(lib, "ComCtl32.lib")

#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;								// current instance
TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name

// Forward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);
WCHAR* MyCreateOpenFile(HWND hwnd);

int APIENTRY _tWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPTSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{

	
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

 	// TODO: Place code here.
	MSG msg;
	HACCEL hAccelTable;

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_NOTEPAD, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow))
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_NOTEPAD));

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_ICON1));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDR_MENU1);
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_ICON1));

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   HWND hWnd;

   hInst = hInstance; // Store instance handle in our global variable

   hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//



//==========================//
//============================================//
HWND g_hwnd;
HWND g_hEdit;
HFONT g_hFont;
COLORREF g_editcolor = RGB(0, 0, 0);
HBRUSH g_hbrbackground = CreateSolidBrush(RGB(255, 255, 255));


WCHAR curfile[MAX_PATH];
bool needsave = false;
bool isopened = false;


//Declare Functions

void LoadFileToEdit();

void SaveTextFileFromEdit();

bool GetFileNameForSave();

void checksave();

void ChooseFontForEdit();



void LoadFileToEdit()
{
	curfile[0] = '\0';
	OPENFILENAME ofn;
	ZeroMemory(&ofn, sizeof(OPENFILENAME));
	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.hwndOwner = g_hwnd;
	ofn.lpstrFilter = L"Text Files(*.txt)\0*.txt\0All File(*.*)\0*.*\0";
	ofn.lpstrFile = NULL;
	ofn.nMaxFile = MAX_PATH;
	ofn.Flags = OFN_EXPLORER | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY;
	ofn.lpstrDefExt = L"txt";
	if (!GetOpenFileName(&ofn))
		return;
	HANDLE hFile;
	bool bsucces = false;
	hFile = CreateFile(curfile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);
	if (hFile != INVALID_HANDLE_VALUE)
	{
		DWORD dwFileSize;
		dwFileSize = GetFileSize(hFile, NULL);
		if (dwFileSize != 0xFFFFFFFF)
		{
			LPSTR tempftext;
			tempftext = (char*)GlobalAlloc(GPTR, dwFileSize + 1);
			if (tempftext != NULL)
			{
				DWORD dwRead;
				if (ReadFile(hFile, tempftext, dwFileSize, &dwRead, NULL))
				{
					tempftext[dwFileSize] = 0;
					if (SetWindowText(g_hEdit, (LPCWSTR)tempftext))
						bsucces = true;
				}
				GlobalFree(tempftext);
			}
		}
		CloseHandle(hFile);
	}
	if (!bsucces)
	{
		MessageBox(g_hwnd, L"The File could not be loaded!!", L"Error", MB_OK | MB_ICONERROR);
		return;
	}
	SetWindowText(g_hwnd, curfile);
	needsave = false;
	isopened = true;
}

void SaveTextFileFromEdit()
{
	HANDLE hFile;
	bool bsucces = false;
	hFile = CreateFile(curfile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	if (hFile != INVALID_HANDLE_VALUE)
	{
		DWORD dwTextLength;
		dwTextLength = GetWindowTextLength(g_hEdit);
		LPSTR pszText;
		DWORD dwBufferSize = dwTextLength + 1;
		pszText = (char*)GlobalAlloc(GPTR, dwBufferSize);
		if (pszText != NULL)
		{
			if (GetWindowText(g_hEdit, (LPWSTR)pszText, dwBufferSize))
			{
				DWORD dwWritten;
				if (WriteFile(hFile, pszText, dwTextLength, &dwWritten, NULL))
					bsucces = true;
			}
			GlobalFree(pszText);
		}

		CloseHandle(hFile);
	}
	if (!bsucces)
	{
		MessageBox(g_hwnd, L"The File could not be saved!!!", L"Error", MB_OK | MB_ICONERROR);
		return;
	}
	isopened = true;
	needsave = false;
}

bool GetFileNameForSave()
{
	OPENFILENAME ofn;
	ZeroMemory(&ofn, sizeof(OPENFILENAME));
	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.hwndOwner = g_hwnd;
	ofn.lpstrFilter = L"Text Files(*.txt)\0*.txt\0All File(*.*)\0*.*\0";
	ofn.lpstrFile = NULL;
	ofn.nMaxFile = MAX_PATH;
	ofn.Flags = OFN_EXPLORER | OFN_PATHMUSTEXIST | OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT;
	ofn.lpstrDefExt = L"txt";
	if (!GetSaveFileName(&ofn))
		return false;
	return true;
}

void checksave()
{
	if (needsave)
	{
		int res;
		res = MessageBox(g_hwnd, L"The File has been changed!!!\nDo you want to save it before continueing?", L"Save File before continueing!!", MB_YESNOCANCEL | MB_ICONINFORMATION);
		if (res == IDCANCEL)
			return;
		if (GetFileNameForSave())
			SaveTextFileFromEdit();

	}
}
void ChooseFontForEdit()
{
	CHOOSEFONT cf = { sizeof(CHOOSEFONT) };
	LOGFONT lf;
	GetObject(g_hFont, sizeof(LOGFONT), &lf);
	cf.Flags = CF_EFFECTS | CF_SCREENFONTS | CF_INITTOLOGFONTSTRUCT;
	cf.hwndOwner = g_hwnd;
	cf.lpLogFont = &lf;
	cf.rgbColors = g_editcolor;
	if (!ChooseFont(&cf))
		return;
	HFONT hf = CreateFontIndirect(&lf);
	if (hf)
	{
		g_hFont = hf;
		SendMessage(g_hEdit, WM_SETFONT, (WPARAM)g_hFont, TRUE);
	}
	g_editcolor = cf.rgbColors;
}
//==========================//





HWND hEdit;
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;
	RECT rect;
	switch (message)
	{
	case WM_SIZE:
		RECT rcClient;
		GetClientRect(hWnd, &rcClient);
		hEdit = GetDlgItem(hWnd, IDC_MAIN_EDIT);
		SetWindowPos(hEdit, NULL, 0, 0, rcClient.right, rcClient.bottom, SWP_NOZORDER);
		break;
	case WM_CREATE:
		//HWND hEdit;

		hEdit = CreateWindowEx(0, L"Edit", nullptr, WS_CHILD | WS_VISIBLE | ES_LEFT | ES_MULTILINE | WS_VSCROLL | ES_AUTOVSCROLL | WS_HSCROLL | ES_AUTOHSCROLL
			, 0, 0, CW_USEDEFAULT, CW_USEDEFAULT, hWnd, (HMENU)IDC_MAIN_EDIT, (HINSTANCE)GetWindowLong(hWnd, GWLP_HINSTANCE), nullptr);
		if (hEdit == NULL)
			MessageBox(hWnd, L"Could not create edit box.", L"Error", MB_OK | MB_ICONERROR);

		HFONT hfDefault;
		hfDefault = (HFONT)GetStockObject(DEFAULT_GUI_FONT);
		SendMessage(hEdit, WM_SETFONT, (WPARAM)hfDefault, MAKELPARAM(FALSE, 0));

		g_hEdit = hEdit;
		g_hFont = hfDefault;
	case WM_COMMAND:
		wmId    = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case ID_FILE_EXIT:
			DestroyWindow(hWnd);
			break;
		case ID_EDIT_CUT:  // khi người bấm vào nút Edit->Cut thì mình sẽ gửi 1 tin nhắn đến lớp edit rằng nó phải cut đi dòng đang tô đen đó.
				SendMessage(hEdit, WM_CUT, 0, 0);
			break;
		case ID_EDIT_COPY:
			SendMessage(hEdit, WM_COPY, 0, 0);
			break;
		case ID_EDIT_DELETE:
				SendMessage(hEdit, WM_CHAR, 8, 0); //DELETE mã ASCII là 8
			break;
		//case ID_FILE_NEW:
		//		SendMessage(hEdit, EM_SETSEL, 0,-1); // chon het CTRL + A
		//		SendMessage(hEdit, WM_CHAR, 8, 0);// Xoa DELETE
		//	break;
		case ID_EDIT_PASTE:
			SendMessage(hEdit, WM_PASTE, 0, 0);
			break;
		case ID_EDIT_UNDO:
			SendMessage(hEdit, WM_UNDO, 0, 0);
			break;
			//=======================//
		case ID_FILE_NEW:
			checksave();
			isopened = false;
			needsave = false;
			SetWindowText(g_hEdit, L"");
			SetWindowText(g_hwnd, L"MyPad 1.0 [Untitled]");
		break;
		case ID_FILE_OPEN:
		{
			checksave();
			LoadFileToEdit();
		}
		break;
		case ID_FILE_SAVE:
		{
			if (needsave)
			{
				if (isopened)
					SaveTextFileFromEdit();
				else if (GetFileNameForSave())
					SaveTextFileFromEdit();
			}
		}
		break;
		case ID_FILE_SAVEAS:
		{
			if (GetFileNameForSave())
				SaveTextFileFromEdit();
		}
		break;
		case IDC_MAIN_EDIT:
			switch (HIWORD(wParam))
			{
			case EN_CHANGE:
				needsave = true;
				break;
			}
			break;
			//=======================//
		}
		break;
	//default:
		//return DefWindowProc(hWnd, message, wParam, lParam);
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		// TODO: Add any drawing code here...
		EndPaint(hWnd, &ps);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return DefWindowProc(hWnd, message, wParam, lParam);

}
WCHAR szFile[260];
WCHAR* MyCreateOpenFile(HWND hwnd)
{
	OPENFILENAMEW ofn = { 0 };
	ofn.lStructSize = sizeof(ofn);
	ofn.hwndOwner = hwnd;
	ofn.lpstrFile = szFile;
	ofn.lpstrFile[0] = L'\0';
	ofn.nMaxFile = sizeof(szFile);
	ofn.lpstrFilter = L"Text Documents (*.txt)\0*.txt\0All Files (*.*)\0*.*\0";
	ofn.nFilterIndex = 2;
	ofn.lpstrFileTitle = NULL;
	ofn.nMaxFileTitle = 0;
	ofn.lpstrInitialDir = NULL;
	ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;
	GetOpenFileName(&ofn);
	return ofn.lpstrFile;
}

